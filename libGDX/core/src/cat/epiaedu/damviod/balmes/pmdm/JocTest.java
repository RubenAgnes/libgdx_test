package cat.epiaedu.damviod.balmes.pmdm;


import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;

public class JocTest implements ApplicationListener , InputProcessor{
	SpriteBatch batch;
	Texture img;
	TextureAtlas textureAtlas;
	Animation animation;
	Rectangle sprite;
	TextureRegion texture;
	float movementSpeed;
	boolean flip;

	float elapsedTime;
	float lastTime;

	Sound motor_idle;
	Sound motor_running;
	long SoundID;
	float rpm = 1;

	@Override
	public void create () {
		batch = new SpriteBatch();
		img = new Texture("background.jpg");
		textureAtlas = new TextureAtlas(Gdx.files.internal("bwoy.atlas"));
		animation = new Animation(1/15f, textureAtlas.getRegions());
		sprite = new Rectangle();
		movementSpeed = 0f;
		Gdx.input.setInputProcessor(this);
		flip = false;
		sprite.x = 400;
		sprite.y = 50;
		texture = new TextureRegion();

		motor_idle = Gdx.audio.newSound(Gdx.files.internal("engine-idle.wav"));
		motor_running = Gdx.audio.newSound(Gdx.files.internal("engine-running.wav"));
		motor_idle.loop();

		motor_idle.play();

		SoundID = motor_running.loop();
		motor_running.stop();
	}

	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void render () {

		Gdx.gl.glClearColor(0, 0, 1, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		sprite.x += movementSpeed * lastTime;

		batch.begin();
		texture.setRegion((TextureRegion)animation.getKeyFrame(elapsedTime,true));
		texture.flip(flip,false);
		elapsedTime += Gdx.graphics.getDeltaTime();
		lastTime = Gdx.graphics.getDeltaTime();
		batch.draw(img, 0, 0, 1000, 1000);
		batch.draw(texture,sprite.x,sprite.y);
		batch.end();

		if(movementSpeed !=0f){
			motor_running.setPitch(SoundID,rpm);
			if(rpm<4)rpm = rpm +0.01f;
		}else if(rpm>1 ){
			motor_running.setPitch(SoundID,rpm);
			if(rpm>1)rpm = rpm -0.1f;
		}else if (rpm<1){
			motor_running.stop();
		}
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}

	@Override
	public void dispose () {
		batch.dispose();
		img.dispose();
	}

	@Override
	public boolean keyDown(int keycode) {

		if(keycode == Input.Keys.LEFT)
		{
			flip = true;
			movementSpeed = -500f;
		}
		if(keycode == Input.Keys.RIGHT)
		{
			flip = false;
			movementSpeed = 500f;
		}
		if(keycode == Input.Keys.SPACE)
		{
			if(flip) {
				movementSpeed = -500f;
			}
			else{ movementSpeed = 500f; }

			SoundID = motor_running.play();
			motor_running.setLooping(SoundID, true);
			rpm = rpm +0.1f;
		}

		return false;
	}

	@Override
	public boolean keyUp(int keycode) {

		movementSpeed = 0f;
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		if(button ==0){
			sprite.x = screenX - texture.getRegionWidth()/2;
			sprite.y = (Gdx.graphics.getHeight() - screenY) - texture.getRegionHeight()/2;
		}
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		return false;
	}
}
