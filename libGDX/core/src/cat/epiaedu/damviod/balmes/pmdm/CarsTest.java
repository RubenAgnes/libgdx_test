package cat.epiaedu.damviod.balmes.pmdm;

/**
 * Created by ruben.agnes on 08/02/2017.
 */
import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.scenes.scene2d.InputEvent;

public class CarsTest implements ApplicationListener, InputProcessor {

    Sound motor_idle;
    Sound motor_running;
    long SoundID;
    float rpm = 1;
    float movement_speed;

    public void create () {
        motor_idle = Gdx.audio.newSound(Gdx.files.internal("engine-idle.wav"));
        motor_running = Gdx.audio.newSound(Gdx.files.internal("engine-running.wav"));
        motor_idle.loop();

        motor_idle.play();

        SoundID = motor_running.loop();
        motor_running.stop();

        movement_speed = 0f;
    }

    @Override
    public void render() {
        if(movement_speed !=0f){
            motor_running.setPitch(SoundID,rpm);
            if(rpm<4)rpm = rpm +0.01f;
        }else if(rpm>1 ){
            motor_running.setPitch(SoundID,rpm);
            if(rpm>1)rpm = rpm -0.1f;
        }else if (rpm<1){
            motor_running.stop();
        }
    }

    @Override
    public boolean keyDown(int keycode){
        if(keycode == Input.Keys.SPACE)
        {
            movement_speed = 500f;
            SoundID = motor_running.play();
            motor_running.setLooping(SoundID, true);
            rpm = rpm +0.1f;
        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {

        movement_speed = 0f;

        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void dispose() {

    }
}
