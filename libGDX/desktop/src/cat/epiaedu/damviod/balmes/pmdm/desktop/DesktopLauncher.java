package cat.epiaedu.damviod.balmes.pmdm.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import cat.epiaedu.damviod.balmes.pmdm.CarsTest;
import cat.epiaedu.damviod.balmes.pmdm.JocTest;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		new LwjglApplication(new JocTest(), config);
	}
}
